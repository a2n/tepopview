//
//  TEPopUpView.h
//  TEPopUpView
//
//  Created by  on 12/4/17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEPopUpView : UIView
{
@private
    CGSize viewSize;
    UIWindow *window;
    UIView *_dialogView;
}
- (id)initWithSize:(CGSize)size;
- (UIView *)dialogView;
@end
